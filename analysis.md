# Improvements

* Create a tailored EKS cluster for real world uses.

* Setup Terraform to accept multiple IPs to configure the Security Group.

* Use Ansible dynamic host.