# Requirements to run this project

## Checkout the source code

Checkout this project and `cd` to its directory.

```
git clone https://gitlab.com:mleem/eintopf.git
cd eintopf
```

You can also fork this project.

## Setup ansible-vault key

`ansible-vault` is the "master key" of this project. It is used to decrypt all sensitive info.

### If you already have an ansible-vault key

If you got the `ansible-vault` by email, follow these steps. If not, go to the next step.

Create the `ansible-vault` file and paste the key in it.

```
vi ansible/_security/decrypt/ansible-vault
```

### If you do not have an ansible-vault key

You will need to create one.

Create `ansible-vault` file.

```
vi ansible/_security/decrypt/ansible-vault
```

Input any alphanumercial password, for example `Abc123!$`. Save the file.

**Hint:** also save this password in a safe place because `ansible-vault` file is "gitignored". You will need to create this file manually everytime you clone this repo.

Create an SSH RSA key.

```
ssh-keygen -f ansible/_security/encrypt/ssh.pem -t rsa -b 4096
```

Leave the passphrase empty.

**Hint:** also save `ssh.pem` and `ssh.pem.pub` in a safe place.

Encrypt them.

```
ansible-vault encrypt ansible/_security/encrypt/ssh.pem
ansible-vault encrypt ansible/_security/encrypt/ssh.pem.pub
```

Now decrypt they keys, they will be used by Ansible.

```
cd ansible/
make decrypt
```

Now you should have the following files:

```
_security/decrypt/ssh.pem
_security/decrypt/ssh.pub
```

Copy the public key to Terraform.

```
cd ..
cp ansible/_security/decrypt/ssh.pub terraform/003-vm-mgmt/files/id_rsa.pub
```

## Setup AWS credentials

On the project's root folder, create a file called `env-var` which will store your AWS credentials.

```
vi env-var
```

The content should be as follows:

```
AWS_ACCESS_KEY_ID=<put your key here>
AWS_SECRET_ACCESS_KEY=<put your scret here>
AWS_DEFAULT_REGION=<put your AWS region here>
```
## Run the docker container

From the project`s root folder, run the following command.

```
docker run -ti --rm \
  --name devops-bootstrap \
  --env-file ./env-var \
  -v $(pwd):/app \
  tadeugr/devops
```

# Provision the infrastructure with Terraform

Create Terraform S3 bucket to store the state.

```
cd /app/terraform/001-backend-s3
terraform init
terraform apply
```

Create Terraform DynamoDB lock.

```
cd /app/terraform/002-backend-lock
terraform init
terraform apply
```

Create a VM that will be used to manage the infrastructure.


```
export TF_VAR_WHAT_IS_MY_IP=$(dig +short myip.opendns.com @resolver1.opendns.com)
cd /app/terraform/003-vm-mgmt
terraform init
terraform apply
```

This command will output `vm-public-ip` which is the Virtual Machine`s public IP. Copy it. It will be used later on Ansible hosts file.

# Setup the infrastructure with Ansible

Change the directory to Ansible.

```
cd /app/ansible/
```

Ansible uses SSH connection to make changes on the server. To do so, it is necessary an RSA key, which is already in this repo, but it is encrypted.

To decrypt the RSA key run the follwing command (note: you must have set up `ansible-vault` file as described before).

```
make decrypt
```

Edit Ansible hosts file.

```
vi hosts 
```

Change the IP on `[mgmt]` section. For example.

```
[mgmt]
18.221.27.123
```

Run the playbook.

```
ansible-playbook playbooks/mgmt.yaml
```

# How to use kubectl

SSH to VM, access the docker container and run kubectl commands.

```
ssh -i ansible/_security/decrypt/ssh.pem ubuntu@PUT.YOUR.VM.IP.HERE
sudo su -
docker exec -it devops /bin/bash
kubectl cluster-info
```

